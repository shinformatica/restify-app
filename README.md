# Restify App

Template padrão para iniciar um projeto Restify com mongoose e jwt.

## Pré-requisitos e ferramentas auxiliares

* MongoDB (manager Robo3T)
* Git
* Node.js
* NPM
* Typescript (global)
* Nodemon (global)
* loadtest (global - teste)
* pm2 (global - produção)
* Restlet Client (chrome) ou Postman

## Start do projeto

Você precisa inicialmente instalar as dependências do package.json:

```
npm install
```

A seguir, execute o compilador do Typescript em modo assistido:

```
tsc -w
```

Coloque o mongoDB em execução e, por fim, execute:

```
nodemon dist/index
```

ou

```
npm start
```