import * as restify from 'restify'
import {Router} from './shared/router'

class IndexRouter extends Router {
  applyRoutes(app: restify.Server){
    app.get('/', (req, res, next) => {
      res.json({message: 'Welcome to Restify API! :D'})
      return next()
    })
  }
}
export const indexRouter = new IndexRouter()