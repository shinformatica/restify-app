import {Server} from './bin/server'
import {userRouter} from './app/users/users.router'
import {indexRouter} from './index.router'

const server = new Server()
const routes = [indexRouter, userRouter]

server.bootstrap(routes).then((server) => {
  console.log(`Server listening on: http://localhost:${server.app.address().port}`)
}).catch((error) => {
  console.log('Server failed to start')
  console.error(error)
  process.exit(1)
})


