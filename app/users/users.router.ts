import * as restify from 'restify'
import {User} from './users.model'
import {ModelRouter} from '../../shared/model-router'
import {authenticate} from '../../safety/handler.authenticate'
import {authorize} from '../../safety/handler.authorize'

class UserRouter extends ModelRouter<User> {
  
  constructor(){
    super(User)
    this.on('beforeRender', (document) => {
      document.password = undefined
    })
  }

  applyRoutes(app: restify.Server) {

    app.get(`${this.basePath}`, [authorize('user'), this.findAll])
    app.get(`${this.basePath}/:id`, [authorize('user'), this.validateId, this.findById])
    app.post(`${this.basePath}`, this.save)
    app.put(`${this.basePath}/:id`, [authorize('user'), this.validateId, this.replace])
    app.patch(`${this.basePath}/:id`, [authorize('user'), this.validateId, this.update])
    app.del(`${this.basePath}/:id`, [authorize('user'), this.validateId, this.delete])

    app.post(`${this.basePath}/authenticate`, authenticate)
  }
}

export const userRouter = new UserRouter()