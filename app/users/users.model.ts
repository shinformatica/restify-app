import * as mongoose from 'mongoose'
import * as bcrypt from 'bcrypt'
import {environment} from '../../shared/environment'

export interface User extends mongoose.Document {
  username: string,
  email: string,
  password: string,
  profiles: string[],
  matches(password: string): boolean,
  hasAny(...profiles: string[]): boolean
}

const schema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    maxlength: 80,
    minlength: 3
  },
  email: {
    type: String,
    unique: true,
    match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    required: true
  },
  password: {
    type: String,
    select: false,
    required: true
  },
  profiles: {
    type: [String],
    required: true,
    default: ['user']
  }
})

schema.methods.matches = function(password: string): boolean {
  return bcrypt.compareSync(password, this.password)
}

schema.methods.hasAny = function(...profiles: string[]): boolean {
  return profiles.some(profile => this.profiles.indexOf(profile) !== -1)
}

const hashPassword = (obj, next) => {
  bcrypt.hash(obj.password, environment.security.saltRounds).then((hash) => {
    obj.password = hash
    next()
  }).catch(next)
}

const saveMiddleware = function(this: User, next) {
  if(!this.isModified('password')){
    next()
  } else {
    hashPassword(this, next)
  }
}

const updateMiddleware = function(this: mongoose.Query<User>, next) {
  if(!this.getUpdate().password){
    next()
  } else {
    hashPassword(this.getUpdate(), next)
  }
}

schema.pre('save', saveMiddleware)
schema.pre('update', updateMiddleware)
schema.pre('findOneAndUpdate', updateMiddleware)

export const User = mongoose.model<User>('User', schema)