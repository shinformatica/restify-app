// $ pm2 start ecosystem.config.js
module.exports = {
  apps: [
    {
      name: "restify-api",
      script: "./dist/index.js",
      instances: 0,
      exec_mode: "cluster",
      watch: true,
      merge_logs: true,
      env: {
        NODE_ENV: "production",
        SERVER_PORT: 8080,
        DB_URL: "mongodb://login:password@localhost/restify-api-production"
      }
    }
  ]
}