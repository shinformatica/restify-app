export const environment = {
  server: { port: process.env.SERVER_PORT || 3000 },
  db: { url: process.env.DB_URL || 'mongodb://localhost/restify-api' },
  security: { 
    saltRounds: process.env.SALT_ROUDS || 10,
    secret: process.env.API_SECRET || 'restify-api-secret',
    enableHTTPS: process.env.ENABLE_HTTPS || false,
    certificate: process.env.CERT_FILE || './safety/keys/cert.pem',
    key: process.env.CERT_KEY || './safety/keys/key.pem',
    token_expire_in: "5m",
    token_cookie_expire_in: 300
  },
  log: {
    level: process.env.LOG_LEVEL || 'debug',
    name: 'restify-api-logger'
  }
}