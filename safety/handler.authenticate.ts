import * as restify from 'restify'
import * as jwt from 'jsonwebtoken'
import {User} from '../app/users/users.model'
import {NotAuthorizedError} from 'restify-errors'
import {environment} from '../shared/environment'

export const authenticate:restify.RequestHandler = (req, res, next) => {
  const { username, password } = req.body
  User.findOne({username}, '+password').then(user => {
     if(user && user.matches(password)) {
      const token = jwt.sign({sub: user.username, iss: 'restify-api'}, environment.security.secret, {expiresIn: environment.security.token_expire_in})
      res.setCookie('Authorization', `Bearer ${token}`, {maxAge: environment.security.token_cookie_expire_in})
      res.json({name: user.username, token: token})
      return next(false)
     } else {
       return next(new NotAuthorizedError('Invalid credentials'))
     }
  }).catch(next)
}