import * as restify from 'restify'
import * as jwt from 'jsonwebtoken'
import {User} from '../app/users/users.model'
import {environment} from '../shared/environment'

export const handlerToken:restify.RequestHandler = (req, res, next) => {
  const token: string = String(extractToken(req))
  if(token){
    jwt.verify(token, environment.security.secret, applyBearer(req, next))
  } else {
    next()
  }
}

function extractToken(req: restify.Request){
  const authorization = req.cookies['Authorization']
  if(authorization){
    const parts: String[] = authorization.split(' ')
    if(parts.length === 2 && parts[0] === 'Bearer'){
      return parts[1]
    }
    return undefined
  }
}

function applyBearer(req: restify.Request, next): (error, decoded) => void {
  return (error, decoded) => {
    if(decoded){
      const username = decoded.sub
      User.findOne({username}).then(user => {
        if(user){
          (<any>req).authenticated = user
        }
        return next()
      }).catch(next)
    } else {
      next()
    }
  }
}