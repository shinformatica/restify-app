import * as restify from 'restify'

export const handlerVersion = restify.plugins.conditionalHandler({
  contentType: 'application/json',
  version: '1.0.0',
  handler: function(req, res, next) { next() }
})