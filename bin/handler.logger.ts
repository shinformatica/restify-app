import * as bunyan from 'bunyan'
import {environment} from '../shared/environment'

export const handlerLogger = bunyan.createLogger({
  name: environment.log.name,
  level: (<any>bunyan).resolveLevel(environment.log.level)
})