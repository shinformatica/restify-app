import * as fs from 'fs'

import * as restify from 'restify'
import * as mongoose from 'mongoose'
import * as cors from 'restify-cors-middleware'
import * as CookieParser from 'restify-cookies'
import {environment} from '../shared/environment'
import {Router} from '../shared/router'
import {handlerLogger} from './handler.logger'
import {handleError} from './handler.error'
import {handlerVersion} from './handler.version'
import {handlerToken} from '../safety/handler.token'

export class Server {
  app:restify.Server

  initializeDb(): Promise<any> {
    (<any> mongoose).Promise = global.Promise
    return mongoose.connect(environment.db.url, {
      useCreateIndex: true,
      useNewUrlParser: true
    })
  }

  initRoutes(routers: Router[]): Promise<any> {
    return new Promise((resolve, reject) => {
      try{
        const options: restify.ServerOptions = { name: 'restify-api', version: '1.0.0', log: handlerLogger }
        if(environment.security.enableHTTPS) {
          options.certificate = fs.readFileSync(environment.security.certificate),
          options.key = fs.readFileSync(environment.security.key)
        }
        this.app = restify.createServer(options)

        const corsOptions: cors.Options = {
          preflightMaxAge: 10,
          origins: ['*'],
          allowHeaders: ['authorization'],
          exposeHeaders: ['x-custom-header'] }
        const corsMiddleware: cors.CorsMiddleware = cors(corsOptions)

        this.app.pre(corsMiddleware.preflight)
        this.app.pre(restify.plugins.requestLogger({log: handlerLogger}))

        this.app.use(corsMiddleware.actual)
        this.app.use(restify.plugins.queryParser())
        this.app.use(restify.plugins.bodyParser())
        this.app.use((<any>CookieParser).parse)
        this.app.use(handlerVersion)
        this.app.use(handlerToken)

        for (let router of routers){ router.applyRoutes(this.app); }
        this.app.listen(environment.server.port, () => {
          resolve(this.app)
        })

        this.app.on('restifyError', handleError)
      } catch(error){ reject(error) }
    })
  }

  bootstrap(routers: Router[] = []): Promise<Server> {
    return this.initializeDb().then(() => this.initRoutes(routers).then(() => this)) 
  }

  shutdown() {
    return mongoose.disconnect().then(() => { this.app.close() })
  }
}